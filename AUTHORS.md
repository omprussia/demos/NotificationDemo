# Authors

* Andrei Begichev, <a.begichev@omp.ru>
  * Maintainer, 2023
  * Developer, 2023
* Nikolay Gubarev, <n.gubarev@omp.ru>
  * Developer, 2023
* Pavel Kazeko
  * Developer, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer 2023
* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2020-2023
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
