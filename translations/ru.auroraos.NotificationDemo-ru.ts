<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="9"/>
        <source>Notification Demo</source>
        <translation>Демо уведомлений</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="24"/>
        <source>Notification Demo</source>
        <translation>Демо уведомлений</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="45"/>
        <source>Notification wake up</source>
        <translation>Уведомление о пробуждении</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>When you click on this button, a notification will appear and the application will end. After clicking on the notification, the application will open again.</source>
        <translation>При нажатии на эту кнопку появится уведомление и приложение закроется. После нажатия на уведомление приложение откроется снова.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>Publish</source>
        <translation>Опубликовать</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="75"/>
        <source>Notification actions</source>
        <translation>Действия по оповещению</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="83"/>
        <source>Delay before publish</source>
        <translation>Задержка перед публикацией</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="84"/>
        <source>For lock screen testing</source>
        <translation>Для тестирования экрана блокировки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Delay</source>
        <translation>Интервал</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>%1 sec</source>
        <translation>%1 сек</translation>
    </message>
</context>
<context>
    <name>NotificationController</name>
    <message>
        <location filename="../src/notification_controller.cpp" line="49"/>
        <source>Answer</source>
        <translation>Ответить</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="50"/>
        <source>Decline</source>
        <translation>Отклонить</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="57"/>
        <location filename="../src/notification_controller.cpp" line="85"/>
        <source>Notification Demo</source>
        <translation>Демо уведомлений</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="59"/>
        <source>Call from:</source>
        <translation>Звонит:</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="60"/>
        <source>Ivan Ivanov</source>
        <translation>Иван Иванов</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="82"/>
        <source>Open app</source>
        <translation>Открыть приложение</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="86"/>
        <source>Press on this notification to open app</source>
        <translation>Нажмите на это уведомление, чтобы открыть приложение</translation>
    </message>
</context>
</TS>
