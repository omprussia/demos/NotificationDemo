<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="9"/>
        <source>Notification Demo</source>
        <translation>Notification Demo</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="24"/>
        <source>Notification Demo</source>
        <translation>Notification Demo</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="45"/>
        <source>Notification wake up</source>
        <translation>Notification wake up</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>When you click on this button, a notification will appear and the application will end. After clicking on the notification, the application will open again.</source>
        <translation>When you click on this button, a notification will appear and the application will end. After clicking on the notification, the application will open again.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>Publish</source>
        <translation>Publish</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="75"/>
        <source>Notification actions</source>
        <translation>Notification actions</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="83"/>
        <source>Delay before publish</source>
        <translation>Delay before publish</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="84"/>
        <source>For lock screen testing</source>
        <translation>For lock screen testing</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Delay</source>
        <translation>Delay</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>%1 sec</source>
        <translation>%1 sec</translation>
    </message>
</context>
<context>
    <name>NotificationController</name>
    <message>
        <location filename="../src/notification_controller.cpp" line="49"/>
        <source>Answer</source>
        <translation>Answer</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="50"/>
        <source>Decline</source>
        <translation>Decline</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="57"/>
        <location filename="../src/notification_controller.cpp" line="85"/>
        <source>Notification Demo</source>
        <translation>Notification Demo</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="59"/>
        <source>Call from:</source>
        <translation>Call from:</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="60"/>
        <source>Ivan Ivanov</source>
        <translation>Ivan Ivanov</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="82"/>
        <source>Open app</source>
        <translation>Open app</translation>
    </message>
    <message>
        <location filename="../src/notification_controller.cpp" line="86"/>
        <source>Press on this notification to open app</source>
        <translation>Press on this notification to open app</translation>
    </message>
</context>
</TS>
