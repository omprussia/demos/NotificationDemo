// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
        }

        Column {
            id: column

            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Notification Demo")
                extraContent.children: [
                    Image {
                        id: headerIcon

                        source: Qt.resolvedUrl("../images/ru.auroraos.NotificationDemo.svg")
                        sourceSize {
                            width: Theme.iconSizeMedium
                            height: Theme.iconSizeMedium
                        }
                        anchors.verticalCenter: parent.verticalCenter
                    }
                ]
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingMedium
                }
                text: qsTr("Notification wake up")
                font.pixelSize: Theme.fontSizeHuge
                color: Theme.primaryColor
            }

            TextArea {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                readOnly: true
                text: qsTr("When you click on this button, a notification will appear and the " + "application will end. After clicking on the notification, the " + "application will open again.")
            }

            ButtonLayout {
                anchors.horizontalCenter: parent.horizontalCenter

                Button {
                    text: qsTr("Publish")

                    onClicked: controller.publishWakeUp()
                }
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingMedium
                }
                text: qsTr("Notification actions")
                font.pixelSize: Theme.fontSizeHuge
                color: Theme.primaryColor
            }

            TextSwitch {
                id: delaySwitch

                text: qsTr("Delay before publish")
                description: qsTr("For lock screen testing")
            }

            Slider {
                id: delaySlider

                enabled: delaySwitch.checked
                opacity: enabled ? 1.0 : 0.0
                height: enabled ? implicitHeight : 0
                width: parent.width
                label: qsTr("Delay")
                valueText: qsTr("%1 sec").arg(value)
                value: 5.0
                minimumValue: 0.0
                maximumValue: 60.0
                stepSize: 1.0

                Behavior on opacity  {
                    FadeAnimator {
                    }
                }
                Behavior on height  {
                    NumberAnimation {
                    }
                }
            }

            ButtonLayout {
                anchors.horizontalCenter: parent.horizontalCenter

                Button {
                    text: qsTr("Publish")

                    onClicked: {
                        if (publishDelay.running)
                            publishDelay.stop();
                        if (delaySwitch.checked && delaySlider.value > 0) {
                            publishDelay.interval = delaySlider.value * 1000;
                            publishDelay.start();
                        } else {
                            controller.publishActions();
                        }
                    }
                }
            }

            Timer {
                id: publishDelay

                onTriggered: controller.publishActions()
            }
        }
    }
}
