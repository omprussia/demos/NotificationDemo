// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    CoverPlaceholder {
        text: qsTr("Notification Demo")
        forceFit: true
        icon.source: Qt.resolvedUrl("../images/ru.auroraos.NotificationDemo.svg")
    }
}
