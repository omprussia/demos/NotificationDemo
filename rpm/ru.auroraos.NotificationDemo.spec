Name:       ru.auroraos.NotificationDemo

Summary:    NotificationDemo
Version:    0.1
Release:    1
Group:      Qt/Qt
License:    BSD-3-Clause
URL:        https://developer.auroraos.ru/open-source
Source0:    %{name}-%{version}.tar.bz2

Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  desktop-file-utils
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(nemonotifications-qt5)

%description
В этом проекте демонстрируются примеры уведомлений
1. Всплывающее уведомление по которому можно открыть экземпляр приложения
2. Всплывающее уведомление со звуком для звонка другого пользователя. Также есть возможность поставить таймер для отложенного вызова уведомления

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png

