// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <auroraapp.h>
#include <QtQuick>

#include "notification_controller.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> app(Aurora::Application::application(argc, argv));
    app->setOrganizationName(QStringLiteral("ru.auroraos"));
    app->setApplicationName(QStringLiteral("NotificationDemo"));
    QScopedPointer<NotificationController> controller(new NotificationController(app.data()));
    controller->showApp();

    return app->exec();
}
