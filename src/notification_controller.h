// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NOTIFICATION_CONTROLLER_H
#define NOTIFICATION_CONTROLLER_H

#include <QObject>
#include <QtCore>
#include <QtDBus>
#include <QtQuick>
#include <QSoundEffect>

#include <nemonotifications-qt5/notification.h>

class NotificationController : public QObject
{
    Q_OBJECT

    Q_CLASSINFO("D-Bus Interface", "ru.omp.NotificationDemo")

public:
    explicit NotificationController(QObject *parent = nullptr);

    void showApp();

private:
    void init();

public slots:
    void publishWakeUp();
    void wakeUp();

    void publishActions();

private slots:
    void actionCallProcess(const QString& name);
    void actionCallFinish(uint reason = Notification::Closed);

private:
    QScopedPointer<QQuickView> m_view;

    QScopedPointer<Notification> m_notifyWakeUp;

    QScopedPointer<Notification> m_notifyCall;
    QScopedPointer<QSoundEffect> m_ringtone;

    bool m_isCallAnswer = false;
};

#endif // NOTIFICATION_CONTROLLER_H
