// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "notification_controller.h"
#include <auroraapp.h>

namespace {
const QString notifyService = QStringLiteral("ru.auroraos.NotificationDemo");
const QString notifyPath = QStringLiteral("/ru/auroraos/NotificationDemo");
const QString notifyInterface = QStringLiteral("ru.auroraos.NotificationDemo");

const QString notifyCallAppIcon = QStringLiteral("qml/images/notification-call.svg");
const QString notifyCallPersonIcon = QStringLiteral("qml/images/notification-contact.svg");
const QString notifyCallRingtonPath = QStringLiteral("qml/sounds/notification-actions.wav");

const QString actionCallAnswerName = QStringLiteral("answer");
const QString actionCallDeclineName = QStringLiteral("decline");
} // namespace

NotificationController::NotificationController(QObject *parent) : QObject(parent)
{
}

void NotificationController::showApp()
{
    if (m_view.isNull()) {
        m_view.reset(Aurora::Application::createView());
        init();
        m_view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/NotificationDemo.qml")));
        m_view->engine()->rootContext()->setContextProperty("controller", this);
        m_view->show();
    } else {
        m_view->raise();
        m_view->showFullScreen();
    }
}

void NotificationController::init()
{
    if (!QDBusConnection::sessionBus().interface()->isServiceRegistered(notifyService)) {
        QDBusConnection connection = QDBusConnection::sessionBus();
        if (!connection.registerService(notifyService))
            qWarning("Unable to register D-Bus service '%s'", qPrintable(notifyService));
        if (!connection.registerObject(notifyPath, this, QDBusConnection::ExportAllSlots))
            qWarning("Unable to register D-Bus object at '%s'", qPrintable(notifyPath));
    }

    QVariantList remoteActions = {
        Notification::remoteAction(actionCallAnswerName, tr("Answer"), notifyService, notifyPath, notifyInterface, "actionCallProcess"),
        Notification::remoteAction(actionCallDeclineName, tr("Decline"), notifyService, notifyPath, notifyInterface, "actionCallProcess")
    };

    // Notification call

    m_notifyCall.reset(new Notification(this));
    m_notifyCall->setAppIcon(Aurora::Application::pathTo(notifyCallAppIcon).toString());
    m_notifyCall->setAppName(tr("Notification Demo"));
    m_notifyCall->setIcon(Aurora::Application::pathTo(notifyCallPersonIcon).toString());
    m_notifyCall->setSummary(tr("Call from:"));
    m_notifyCall->setBody(tr("Ivan Ivanov"));
    m_notifyCall->setUrgency(Notification::Critical);
    m_notifyCall->setIsTransient(false);
    m_notifyCall->setItemCount(1);
    m_notifyCall->setExpireTimeout(1000);
    m_notifyCall->setRemoteActions(remoteActions);
    m_notifyCall->setHintValue("x-aurora-essential", QVariant(true)); // Shows the notification already expanded and doesn't allow the user to close it
    m_notifyCall->setHintValue("x-aurora-silent-actions", QStringList("decline")); // Don't need to unlock the device when tapping on "Decline"
    // Won't close the notification after the action button is tapped.
    // !!!WARNING!!! The application takes full responsibility of the notification lifetime!
    m_notifyCall->setHintValue("resident", QVariant(true));

    m_ringtone.reset(new QSoundEffect);
    m_ringtone->setSource(Aurora::Application::pathTo(notifyCallRingtonPath));
    m_ringtone->setLoopCount(QSoundEffect::Infinite);
    m_ringtone->setVolume(1.0);

    connect(m_notifyCall.data(), &Notification::actionInvoked, this, &NotificationController::actionCallProcess);
    connect(m_notifyCall.data(), &Notification::closed, this, &NotificationController::actionCallFinish);

    // Notification wake up

    QVariant actionWakeUp{ Notification::remoteAction(QStringLiteral("default"), tr("Open app"), notifyService, notifyPath, notifyInterface, "wakeUp") };

    m_notifyWakeUp.reset(new Notification(nullptr));
    m_notifyWakeUp->setAppName(tr("Notification Demo"));
    m_notifyWakeUp->setBody(tr("Press on this notification to open app"));
    m_notifyWakeUp->setUrgency(Notification::Normal);
    m_notifyWakeUp->setIsTransient(false);
    m_notifyWakeUp->setItemCount(1);
    m_notifyWakeUp->setExpireTimeout(10000);
    m_notifyWakeUp->setRemoteAction(actionWakeUp);
}

void NotificationController::publishWakeUp()
{
    m_notifyWakeUp->publish();
    qApp->exit();
}

void NotificationController::wakeUp()
{
    showApp();
}

void NotificationController::publishActions()
{
    if (m_view.isNull())
        return;

    m_notifyCall->publish();
    m_ringtone->play();
}

void NotificationController::actionCallProcess(const QString &name)
{
    if (m_view.isNull())
        return;

    m_isCallAnswer = name.compare(actionCallAnswerName) == 0 ? true : false;
    actionCallFinish(Notification::Closed);
}

void NotificationController::actionCallFinish(uint reason)
{
    if (m_view.isNull())
        return;

    if (reason == Notification::DismissedByUser)
        qWarning("Notification dismissed by user!!!");
    else if (reason == Notification::Expired)
        qWarning("Notification expired!!!");

    m_ringtone->stop();
    if (!m_view->isVisible() && m_isCallAnswer)
        showApp();
}
