# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.NotificationDemo

QT += qml quick dbus multimedia
CONFIG += \
    auroraapp

PKGCONFIG += \
nemonotifications-qt5

SOURCES += \
    src/main.cpp \
    src/notification_controller.cpp

HEADERS += \
    src/notification_controller.h

DISTFILES += \
    rpm/ru.auroraos.NotificationDemo.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \
    README.ru.md

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.NotificationDemo.ts \
    translations/ru.auroraos.NotificationDemo-ru.ts \
