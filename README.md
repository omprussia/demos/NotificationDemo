# Notification Demo

This project demonstrates example notifications:
1. Pop-up notification that allows you to open an application instance.
2. Pop-up notification with sound for calling another user. It is also possible to set a timer for a delayed notification call.
    By timer, you can raise a notification to accept a call; if the call is accepted, you will be asked to unlock the screen and the application will open.
    If the call is rejected, nothing will happen. Link to notification documentation: https://developer.auroraos.ru/doc/software_development/reference/notifications

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-CLAUSE.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.NotificationDemo.pro](ru.auroraos.NotificationDemo.pro)** file	describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[images](qml/images)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[sounds](qml/sounds)** directory contains the additional custom sounds.
  * **[NotificationDemo.qml](qml/NotificationDemo.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.NotificationDemo.spec](rpm/ru.auroraos.NotificationDemo.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
  * **[notification_contoller.h](src/notification_contoller.h)** contains an interface declaration for managing notifications.
  * **[notification_contoller.cpp](src/notification_contoller.cpp)** contains an interface definition for managing notifications.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.NotificationDemo.desktop](ru.auroraos.NotificationDemo.desktop)** file defines the display and parameters for launching the application.

## Compatibility

The supported OS version is Aurora 4.0.2.249.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
